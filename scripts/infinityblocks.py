from pyspades.constants import FALL_KILL

def do_restock(p):
	p.old_hp = p.hp
	p.old_grenades = p.grenades

	p.refill();

	p.grenades = p.old_grenades;
	p.set_hp(p.old_hp, kill_type=FALL_KILL);

def apply_script(protocol, connection, config):
	class InfiConnec(connection):
		old_grenades = 3
		old_hp = 100
		def on_block_build(self, x,y,z):
			do_restock(self)
			return connection.on_block_build(self, x, y, z)

		def on_line_build(self, points):
			do_restock(self)
			return connection.on_line_build(self, points)
	return protocol, InfiConnec